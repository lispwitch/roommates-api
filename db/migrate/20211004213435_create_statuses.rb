class CreateStatuses < ActiveRecord::Migration[6.1]
  def change
    create_table :statuses do |t|
      t.text :username
      t.text :mood
      t.text :emoji
      t.text :status

      t.timestamps
    end
  end
end
