class CreateAccounts < ActiveRecord::Migration[6.1]
  def change
    create_table :accounts do |t|
      t.text :username
      t.text :friends

      t.timestamps
    end
    add_index :accounts, :username
  end
end
