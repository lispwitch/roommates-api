class AddPassword < ActiveRecord::Migration[6.1]
  def change
    change_column_default :accounts,
                          :friends,
                          from: nil,
                          to: "[]"
    change_table :accounts do |t|
      t.string :password
    end
  end
end
