module Constants
  USERNAME_REGEX = /\A[[:alnum:]]+\z/
  USERNAME_ERROR = "%{value} contains invalid characters"

  TOKEN_BITS = 256
end
