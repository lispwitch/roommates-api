class Account < ApplicationRecord
  include ActiveModel::Serializers::JSON
  include Constants

  has_many :statuses
  has_secure_password :password
  has_secure_token :auth_token, length: TOKEN_BITS

  validates :username,
            presence: { strict: true },
            uniqueness: true,
            format: { with: USERNAME_REGEX,
                      message: USERNAME_ERROR
                    }

  # This is chosen by the client, not the user.
  validates :password,
            presence: { strict: true }

  validates :friends,
            presence: { strict: true }

  serialize :friends, JSON
end
