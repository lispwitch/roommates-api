class Status < ApplicationRecord
  include Constants
  # TODO: Uncomment when Accounts interface is stable
  # belongs_to :account

  validates :username,
            presence: { strict: true },
            format: { with: USERNAME_REGEX,
                      message: "%{value} contains invalid characters"
                    }

  validates :mood,
            presence: { strict: true },
            inclusion: { in: %w{red yellow green},
                         message: "%{value} is not a valid mood"
                       }
  validates :emoji,
            presence: { strict: true }

  scope :latest_status, ->(username) {
    where(username: username)&.order(id: :desc)&.limit(1)
  }
end
