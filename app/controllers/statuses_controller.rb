class StatusesController < ApplicationController
  # TODO: Almost definitely we can abstract a lot of this behaviour
  def new
    json = JSON.parse(request.raw_post || "{}")
    json[:username] = params[:id]

    @status = Status.create!(json)
    @status.save
    render json: @status.as_json(except: %i{updated_at id}), status: :created
  end

  def show
    # TODO: Add "user on target's friends list" constraint, right now
    # this just gets OUR status, which is fine until the Accounts layer is up.
    @status = Status.latest_status(params[:id])
    render json: @status.as_json(), status: :created
  end
end
