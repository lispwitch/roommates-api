class ApplicationController < ActionController::API
  rescue_from JSON::JSONError, with: :bad_request
  rescue_from ActiveModel::StrictValidationFailed, with: :bad_request
  rescue_from ActiveRecord::RecordInvalid, with: :bad_request
  rescue_from ActiveRecord::RecordNotSaved, with: :server_error

  def bad_request(exception)
    render json: { error: exception.message }, status: :bad_request
  end

  def server_error(exception)
    render json: { error: exception.message }, status: :server_error
  end
end
