class AccountsController < ApplicationController
  def validate_username
    case params[:name]
    when /\A[[:alnum:]]+\z/
      render json: true, status: :ok
    when nil
      render status: :bad_request
    else
      render json: false, status: :ok
    end
  end

  def new
    json = JSON.parse(request.raw_post || {})

    @account = Account.create!(json)
    @account.save
    render json: @account.as_json(), status: :created
  end
end
