Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # Accounts routes
  get "/accounts/check_username", to: "accounts#validate_username"
  post "/accounts/create", to: "accounts#new"

  # Status routes
  post "/status/:id/new", to: "statuses#new"
  get "/status/:id/show", to: "statuses#show"
end
